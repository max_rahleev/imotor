//
//  PopoverInfoTableViewController.h
//  iMotor
//
//  Created by Maksim Rakhleev on 04.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ItemSelectedBlock)(BaseModel *model);
typedef void(^AddNewItemBlock)(NSString *name);

@interface PopoverInfoTableViewController : UITableViewController

@property (strong, nonatomic) NSString *navigationItemTitle;
@property (strong, nonatomic) NSString *sectionTitle;
@property (strong, nonatomic) NSString *addNewItemTitle;
@property (strong, nonatomic) NSArray *contentArray;
@property (copy, nonatomic) ItemSelectedBlock didSelectBlock;
@property (copy, nonatomic) AddNewItemBlock addNewItemBlock;

@end
