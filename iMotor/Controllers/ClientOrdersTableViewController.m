//
//  ClientOrdersTableViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "ClientOrdersTableViewController.h"
#import "ClientOrderTableViewCell.h"

@interface ClientOrdersTableViewController ()
@property (strong, nonatomic) NSArray *orders;
@end

@implementation ClientOrdersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    [self loadOrders];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.orders.count;;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ClientOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClientOrderTableViewCell"];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"ClientOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"ClientOrderTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"ClientOrderTableViewCell" forIndexPath:indexPath];
    }
    
    Order *order = [self.orders objectAtIndex:indexPath.row];
    [cell configureCellWithOrder:order];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 150.f;
}


#pragma mark - Load Data

- (void)loadOrders {
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:orderTableName inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
    [request setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"client.email == %@", COMMON.currentUser.email];
    [request setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    [request setSortDescriptors:@[sortDescriptor]];
    
    self.orders = [CORE_DATA_MANAGER.managedObjectContext executeFetchRequest:request error:nil];
    
    [self.tableView reloadData];
}


@end
