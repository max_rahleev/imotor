//
//  LoginViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Авторизация";
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self.view action:@selector(endEditing:)]];
}


#pragma mark- UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:self.loginTextField])
        [self.passwordTextField becomeFirstResponder];
    else
        [self actionLoginButtonClicked:nil];
    
    return YES;
}


#pragma mark - Helpers

- (void)createValid:(UITextField *)textField {
    
    textField.layer.borderColor = [UIColor clearColor].CGColor;
}

- (void)createNotValid:(UITextField *)textField {
    
    textField.layer.borderWidth = 0.5f;
    textField.layer.borderColor = [UIColor redColor].CGColor;
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 123) {
        [COMMON.leftMenuTableViewController.tableView reloadData];
        [COMMON.leftMenuTableViewController.tableView.delegate tableView:COMMON.leftMenuTableViewController.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}


#pragma mark - IBActions

- (IBAction)actionLoginButtonClicked:(UIButton *)sender {
    
    NSString *login = self.loginTextField.text;
    NSString *password = self.passwordTextField.text;
    if ([login isEqualToString:@""] || [password isEqualToString:@""])
    {
        [login isEqualToString:@""] ? [self createNotValid:self.loginTextField] : [self createValid:self.loginTextField];
        [password isEqualToString:@""]? [self createNotValid:self.passwordTextField] : [self createValid:self.passwordTextField];
    }
    else
    {
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:userTableName
                                                  inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
        [request setEntity:entity];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(email == %@ OR login == %@ OR phone == %@) AND password = %@", login, login, login, password];
        [request setPredicate:predicate];
        [request setFetchLimit:1];

        if ([CORE_DATA_MANAGER.managedObjectContext countForFetchRequest:request error:nil]) {
            COMMON.currentUser = (User *)[CORE_DATA_MANAGER.managedObjectContext executeFetchRequest:request error:nil].firstObject;
            COMMON.userLoggedIn = YES;
            UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"Авторизация прошла успешно"
                                                                  message:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
            successAlert.tag = 123;
            [successAlert show];
        } else {
            [[[UIAlertView alloc]initWithTitle:@"Пользователя с введенными параметрами не существует"
                                       message:@"Попробуйте еще раз, либо зарегистрируйтесь"
                                      delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil] show];
        }
    }
}

- (IBAction)actionSignupButtonClicked:(UIButton *)sender {
    
}


@end
