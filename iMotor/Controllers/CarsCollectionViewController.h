//
//  CarsCollectionViewController.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarsCollectionViewController : UICollectionViewController

@end
