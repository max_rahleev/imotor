//
//  AddNewCarTableTableViewController.h
//  iMotor
//
//  Created by Maksim Rakhleev on 04.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewCarTableTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITableViewCell *brandCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *bodytypeCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *colourCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *kppCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *photoCell;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;

@property (weak, nonatomic) IBOutlet UITextField *modelTextField;
@property (weak, nonatomic) IBOutlet UITextField *graduationYearTextField;
@property (weak, nonatomic) IBOutlet UITextField *engineCapacityTextField;
@property (weak, nonatomic) IBOutlet UITextField *powerTextField;
@property (weak, nonatomic) IBOutlet UITextField *countTextField;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;

@property (weak, nonatomic) IBOutlet UILabel *modelLabel;
@property (weak, nonatomic) IBOutlet UILabel *graduationYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *engineCapacityLabel;
@property (weak, nonatomic) IBOutlet UILabel *powerLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *chooseExistingPhotoButton;
@property (weak, nonatomic) IBOutlet UIScrollView *photosScrollView;

- (IBAction)actionTakePhotoButtonClicked:(UIButton *)sender;
- (IBAction)actionChoosePhotoButtonClicked:(UIButton *)sender;
- (IBAction)actionAddAutoButtonClicked:(UIButton *)sender;

@end
