//
//  CreateOrderTableViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "CreateOrderTableViewController.h"

@interface CreateOrderTableViewController ()

@end

@implementation CreateOrderTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Информация о заказе";
    
    self.brandCell.detailTextLabel.text          = self.model.brand.name;
    self.modelCell.detailTextLabel.text          = self.model.name;
    self.bodytypeCell.detailTextLabel.text       = self.model.bodyType.name;
    self.colourCell.detailTextLabel.text         = self.model.colour.name;
    self.kppCell.detailTextLabel.text            = self.model.kpp.name;
    self.graduationYearCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", self.model.graduationYear];
    self.engineCapasityCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", self.model.engineCapasity];
    self.powerCell.detailTextLabel.text          = [NSString stringWithFormat:@"%@", self.model.power];
    self.priceCell.detailTextLabel.text          = [NSString stringWithFormat:@"%@$", self.model.price];
    
    self.firstNameCell.detailTextLabel.text  = COMMON.currentUser.firstName;
    self.lastNameCell.detailTextLabel.text   = COMMON.currentUser.lastName;
    self.middleNameCell.detailTextLabel.text = COMMON.currentUser.middleName;
    self.emailCell.detailTextLabel.text      = COMMON.currentUser.email;
    self.phoneCell.detailTextLabel.text      = COMMON.currentUser.phone;
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 123 && buttonIndex == 1)
    {
        Order *entity = (Order *)[NSEntityDescription insertNewObjectForEntityForName:orderTableName inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
        
        entity.date   = [NSDate date];
        entity.client = COMMON.currentUser;
        entity.model  = self.model;
        entity.status = [CORE_DATA_MANAGER orderStatusWithStatus:StatusOfOrderOrdered];
        
        if ([CORE_DATA_MANAGER.managedObjectContext save:nil])
        {
            UIAlertView *orderAlert =[[UIAlertView alloc]initWithTitle:@"Ваш заказ успешно отправлен"
                                                               message:@"Статус заказа можно отслеживать в списке заказов"
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            orderAlert.tag = 111;
            [orderAlert show];
        }
    } else if (alertView.tag == 111)
    {
        [COMMON.leftMenuTableViewController.tableView.delegate tableView:COMMON.leftMenuTableViewController.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    }
}


#pragma mark - IBActions

- (IBAction)actionCreateOrderButtonClicked:(UIButton *)sender {
    
    UIAlertView *orderAlert =[[UIAlertView alloc]initWithTitle:@"Отправить заказ?"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"Отмена"
                                             otherButtonTitles:@"Да", nil];
    orderAlert.tag = 123;
    [orderAlert show];
}


@end
