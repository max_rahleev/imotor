//
//  CarsCollectionViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "CarsCollectionViewController.h"
#import "AutoCollectionViewCell.h"

@interface CarsCollectionViewController () <UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) NSArray *cars;
@end

@implementation CarsCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Автомобили в наличии";
    
    if (IS_USER_LOGGED_IN && (IS_ADMIN || IS_MANAGER))
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                              target:self
                                                                                              action:@selector(actionAddNewCarButtonClicked:)];
    
    // Register cell classes
    [self.collectionView registerNib:[UINib nibWithNibName:@"AutoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"AutoCollectionViewCell"];
    
    [self loadCars];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadCars];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    [self.collectionView reloadData];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.cars.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AutoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AutoCollectionViewCell" forIndexPath:indexPath];
    
    if (!cell) {
        [collectionView registerNib:[UINib nibWithNibName:@"AutoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"AutoCollectionViewCell"];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AutoCollectionViewCell" forIndexPath:indexPath];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.parentViewController = self;
    
    NSInteger rowNumber = lroundf(CGRectGetMidY(cell.frame)) / 200;
    
    if (rowNumber % 2)
    {
        if (CGRectGetMidX(cell.frame) < CGRectGetMidX(self.view.bounds))
            cell.backgroundColor = [UIColor colorWithRed:216/255.f green:216/255.f blue:216/255.f alpha:1.f];
    } else {
        if (CGRectGetMidX(cell.frame) > CGRectGetMidX(self.view.bounds))
            cell.backgroundColor = [UIColor colorWithRed:216/255.f green:216/255.f blue:216/255.f alpha:1.f];
    }
    
    Model *model = [self.cars objectAtIndex:indexPath.row];
    [cell configureCellWithModel:model];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(CGRectGetMidX(self.view.bounds), 200.f);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.f;
}


#pragma mark - Actions

- (void)actionAddNewCarButtonClicked:(UIBarButtonItem *)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *addNewCarVC = [storyboard instantiateViewControllerWithIdentifier:@"AddNewCarTableTableViewController"];
    [self.navigationController pushViewController:addNewCarVC animated:YES];
}


#pragma mark - Load Data

- (void)loadCars {
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:modelTableName
                                              inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
    
    [request setEntity:entity];
    [request setFetchBatchSize:10];
    [request setIncludesPropertyValues:NO];
    
    NSSortDescriptor *brandSortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"brand.name" ascending:YES];
    NSSortDescriptor *modelNameSortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"name" ascending:YES];
    [request setSortDescriptors:@[brandSortDescriptor, modelNameSortDescriptor]];
    
    self.cars = [CORE_DATA_MANAGER.managedObjectContext executeFetchRequest:request error:nil];
    [self.collectionView reloadData];
}

@end
