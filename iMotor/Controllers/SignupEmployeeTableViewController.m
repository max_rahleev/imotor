//
//  SignupEmployeeTableViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "SignupEmployeeTableViewController.h"

@interface SignupEmployeeTableViewController ()
@property (strong, nonatomic) UIDatePicker *datePicker;
@end

@implementation SignupEmployeeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Регистрация нового сотрудника";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Очистить" style:UIBarButtonItemStylePlain target:self action:@selector(actionClearFieldsButtonClicked:)];
    
    self.datePicker = [[UIDatePicker alloc]init];
    self.datePicker.date = [NSDate date];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    [self.datePicker addTarget:self action:@selector(actionDataPickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.yearOfBidthcell.infoTextField setInputView:self.datePicker];
    
    //'Done' btn for TF
    UIToolbar* phoneKeyboardDoneButtonView = [[UIToolbar alloc] init];
    [phoneKeyboardDoneButtonView sizeToFit];
    UIBarButtonItem *flexiblSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:nil
                                                                                  action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self.view
                                                                  action:@selector(endEditing:)];
    [phoneKeyboardDoneButtonView setItems:@[flexiblSpace, doneButton]];
    self.yearOfBidthcell.infoTextField.inputAccessoryView = phoneKeyboardDoneButtonView;
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self.view action:@selector(endEditing:)]];
    
}

#pragma mark - Helpers

- (BOOL)validateData {
    
    BOOL flag = YES;
    
    for (UITextField *tf in self.textFields)
    {
        if ([tf.text isEqualToString:@""])
        {
            [self nonValidTextField:tf];
            flag = NO;
        } else
            [self validTextField:tf];
    }
    
    if (![COMMON validateEmail:self.emailCell.infoTextField.text]) {
        [self nonValidTextField:self.emailCell.infoTextField];
        flag = NO;
    } else {
        [self validTextField:self.emailCell.infoTextField];
    }
    
    NSString *pass = self.passwordCell.infoTextField.text;
    NSString *repeatPass = self.passwordRepeatCell.infoTextField.text;
    
    if ([pass isEqualToString:repeatPass] && ![pass isEqualToString:@""] && ![repeatPass isEqualToString:@""] && pass.length >= 6) {
        [self validTextField:self.passwordCell.infoTextField];
        [self validTextField:self.passwordRepeatCell.infoTextField];
    } else {
        [self nonValidTextField:self.passwordCell.infoTextField];
        [self nonValidTextField:self.passwordRepeatCell.infoTextField];
        flag = NO;
    }
    
    return flag;
}

- (void)validTextField:(UITextField *)textField {
    
    textField.layer.borderColor = [UIColor clearColor].CGColor;
}

- (void)nonValidTextField:(UITextField *)textField {
    
    textField.layer.borderWidth = 0.5f;
    textField.layer.borderColor = [UIColor redColor].CGColor;
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 123)
    {
        if (buttonIndex == 1)
        {
            NSFetchRequest *checkUserRequest = [[NSFetchRequest alloc]init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:userTableName
                                                      inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
            [checkUserRequest setEntity:entity];
            NSPredicate *checkPredicate = [NSPredicate predicateWithFormat:@"email == %@ OR phone == %@ OR login == %@",
                                           self.emailCell.infoTextField.text, self.phoneCell.infoTextField.text, self.loginCell.infoTextField.text];
            [checkUserRequest setPredicate:checkPredicate];
            NSInteger countOfUserWithEnteredInfo = [CORE_DATA_MANAGER.managedObjectContext countForFetchRequest:checkUserRequest error:nil];
            
            if (!countOfUserWithEnteredInfo)
            {
                User *entity = (User *)[NSEntityDescription insertNewObjectForEntityForName:userTableName inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
                
                entity.firstName      = [self.firstNameCell.infoTextField.text capitalizedString];
                entity.lastName       = [self.lastNameCell.infoTextField.text capitalizedString];
                entity.middleName     = [self.middleNameCell.infoTextField.text capitalizedString];
                entity.email          = self.emailCell.infoTextField.text;
                entity.login          = self.loginCell.infoTextField.text;
                entity.phone          = self.phoneCell.infoTextField.text;
                entity.dateOfBirth    = self.datePicker.date;
                entity.passportNumber = self.passportNumberCell.infoTextField.text;
                entity.password       = self.passwordCell.infoTextField.text;
                
                if (self.userTypeSegmentedControl.selectedSegmentIndex == 0)
                    entity.type = [CORE_DATA_MANAGER userTypeWithType:UserRoleAdmin];
                else
                    entity.type = [CORE_DATA_MANAGER userTypeWithType:UserRoleManager];
                
                if ([CORE_DATA_MANAGER.managedObjectContext save:nil]) {
                    UIAlertView *signupSuccessAlert = [[UIAlertView alloc]initWithTitle:@"Регистрация нового сотрудника прошла успешно"
                                                                                message:nil
                                                                               delegate:self
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                    signupSuccessAlert.tag = 321;
                    [signupSuccessAlert show];
                }
            } else {
                [[[UIAlertView alloc]initWithTitle:@"Пользователь с одним или несолькими введенными параметрами уже существует"
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil] show];
            }
        }
    } else if (alertView.tag == 321) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger index = [self.textFields indexOfObject:textField];
    if (index < self.textFields.count - 1)
        [[self.textFields objectAtIndex:index + 1]becomeFirstResponder];
    else
        [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Actions

- (void)actionClearFieldsButtonClicked:(UIBarButtonItem *)sender {
    
    for (UITextField *tf in self.textFields)
        tf.text = nil;
}

- (void)actionDataPickerValueChanged:(UIDatePicker *)sender {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"d MMMM yyyy'г.'"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    
    self.yearOfBidthcell.infoTextField.text = [dateFormatter stringFromDate:sender.date];
}


#pragma mark - IBActions

- (IBAction)actionSignUpEmployeeButtonClicked:(UIButton *)sender {
    
    if ([self validateData])
    {
        UIAlertView *signUpAlert = [[UIAlertView alloc]initWithTitle:@"Зарагистрировать клиента с введенными полями?"
                                                             message:nil
                                                            delegate:self
                                                   cancelButtonTitle:@"Отмена"
                                                   otherButtonTitles:@"Да", nil];
        signUpAlert.tag = 123;
        [signUpAlert show];
    }
}


@end
