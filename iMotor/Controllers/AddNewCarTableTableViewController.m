//
//  AddNewCarTableTableViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 04.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "AddNewCarTableTableViewController.h"
#import "CoreDataManager.h"
#import "PopoverInfoTableViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+Saving.h"

@interface AddNewCarTableTableViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) BaseModel *brand;
@property (strong, nonatomic) BaseModel *bodyType;
@property (strong, nonatomic) BaseModel *kpp;
@property (strong, nonatomic) BaseModel *colour;

@property (strong, nonatomic) NSMutableDictionary *photosDict;
@property (strong, nonatomic) UIImage *photoToDeleting;

@property (strong, nonatomic) UIImagePickerController *imagePicker;

@end

@implementation AddNewCarTableTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.photosDict = [NSMutableDictionary dictionary];
    
    self.navigationItem.title = @"Добавление автомобиля в салон";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Очистить"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(actionClearFieldsButtonClicked:)];
    
    self.photosScrollView.layer.borderWidth = 0.5f;
    self.photosScrollView.layer.borderColor = [UIColor lightGrayColor].CGColor;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.view endEditing:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isEqual:self.brandCell]) {
        [self popoverControllerForCellAtIndexPath:indexPath
                                        tableName:brandTableName
                                     sectionTitle:@"Выберите бренд"
                                     newItemTitle:@"Введите название нового бренда"
                                     navItemTitle:@"Бренд"
                                didSelectPropName:@"brand"];
    } else if ([cell isEqual:self.bodytypeCell]) {
        [self popoverControllerForCellAtIndexPath:indexPath
                                        tableName:bodyTypeTableName
                                     sectionTitle:@"Выберите тип кузова"
                                     newItemTitle:@"Введите название типа кузова"
                                     navItemTitle:@"Тип кузова"
                                didSelectPropName:@"bodyType"];
    } else if ([cell isEqual:self.kppCell]) {
        [self popoverControllerForCellAtIndexPath:indexPath
                                        tableName:kppTableName
                                     sectionTitle:@"Выберите тип КПП"
                                     newItemTitle:@"Введите название новой КПП"
                                     navItemTitle:@"КПП"
                                didSelectPropName:@"kpp"];
    } else if ([cell isEqual:self.colourCell]) {
        [self popoverControllerForCellAtIndexPath:indexPath
                                        tableName:colourTableName
                                     sectionTitle:@"Выберите цвет"
                                     newItemTitle:@"Введите название нового цвета"
                                     navItemTitle:@"Цвет"
                                didSelectPropName:@"colour"];
    } else {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


#pragma mark - Configure popover info controllers

- (UIPopoverController *)popoverControllerForCellAtIndexPath:(NSIndexPath *)indexPath
                                                   tableName:(NSString *)tableName
                                                sectionTitle:(NSString *)sectionTitle
                                                newItemTitle:(NSString *)newItemTitle
                                                navItemTitle:(NSString *)navItemTitle
                                           didSelectPropName:(NSString *)propName {
    
    PopoverInfoTableViewController *popoverContentTableVC = [[PopoverInfoTableViewController alloc]initWithStyle:UITableViewStyleGrouped];
    popoverContentTableVC.sectionTitle = sectionTitle;
    popoverContentTableVC.addNewItemTitle = newItemTitle;
    popoverContentTableVC.navigationItemTitle = navItemTitle;
    popoverContentTableVC.contentArray = [CORE_DATA_MANAGER readAllDataFromTableWithName:tableName];
    
    popoverContentTableVC.didSelectBlock = ^(BaseModel *model) {
        [self setValue:model forKey:propName];
        [self.tableView cellForRowAtIndexPath:indexPath].detailTextLabel.text = model.name;
    };
    
    __weak typeof(popoverContentTableVC) weakPopoverInfoVC = popoverContentTableVC;
    popoverContentTableVC.addNewItemBlock = ^(NSString *name) {
        [CORE_DATA_MANAGER addItemWithName:name inTableWithTitle:tableName];
        weakPopoverInfoVC.contentArray = [CORE_DATA_MANAGER readAllDataFromTableWithName:tableName];
    };
    
    UIPopoverController *popover = [[UIPopoverController alloc]initWithContentViewController:[[UINavigationController alloc]initWithRootViewController:popoverContentTableVC]];
    [popover setPopoverContentSize:CGSizeMake(320.f, 500.f) animated:YES];
    
    CGRect aFrame = [self.tableView rectForRowAtIndexPath:indexPath];
    [popover presentPopoverFromRect:[self.tableView convertRect:aFrame toView:self.view] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    
    return popover;
}


#pragma mark - UITextFieldDelegate



#pragma mark - Configure photos scroll view

- (void)configurePhotosScrollView {
    
    CGFloat height = CGRectGetHeight(self.photosScrollView.frame);
    
    NSMutableArray *photoImageViews = [NSMutableArray array];
    
    for (UIImage *image in [self.photosDict allValues]) {
        
        CGFloat aspectRatio = image.size.width / image.size.height;
        
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, height * aspectRatio, height)];
        imageView.image = image;
        
        imageView.userInteractionEnabled = YES;
        UILongPressGestureRecognizer *swipeGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(actionDeletePhoto:)];
        [imageView addGestureRecognizer:swipeGesture];
        
        [photoImageViews addObject:imageView];
    }
    
    for (UIView *view in self.photosScrollView.subviews)
        [view removeFromSuperview];
    
    self.photosScrollView.contentSize = CGSizeZero;
    
    NSInteger originX = 0;
    
    for (UIView *item in photoImageViews)
    {
        CGRect itemFrame = item.frame;
        itemFrame.origin.x = originX - item.layer.borderWidth;
        itemFrame.size.height = height;
        item.frame = itemFrame;
        
        [self.photosScrollView addSubview:item];
        
        originX += CGRectGetWidth(item.frame);
        
        self.photosScrollView.contentSize = CGSizeMake(originX, height);
    }
    
    self.photosScrollView.contentSize = CGSizeMake(self.photosScrollView.contentSize.width/* + self.frame.origin.x*/, height);
}


#pragma mark - Helpers

- (void)showImagePickerWithSoutceType:(UIImagePickerControllerSourceType)sourceType {
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.sourceType          = sourceType;
    self.imagePicker.delegate            = self;
    self.imagePicker.allowsEditing       = NO;
    self.imagePicker.navigationBarHidden = YES;
    self.imagePicker.toolbarHidden       = YES;

    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (BOOL)validateData {
    
    NSCharacterSet *notNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    BOOL flag = YES;
    if (!self.brand) {
        self.brandCell.textLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.brandCell.textLabel.textColor = [UIColor blackColor];
    
    if ([self.modelTextField.text isEqual:@""]) {
        self.modelLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.modelLabel.textColor = [UIColor blackColor];
    
    if (!self.bodyType) {
        self.bodytypeCell.textLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.bodytypeCell.textLabel.textColor = [UIColor blackColor];
    
    if (!self.kpp) {
        self.kppCell.textLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.kppCell.textLabel.textColor = [UIColor blackColor];
    
    if (!self.colour)  {
        self.colourCell.textLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.colourCell.textLabel.textColor = [UIColor blackColor];
    
    if ([self.countTextField.text isEqualToString:@""] && [self.countTextField.text rangeOfCharacterFromSet:notNumberSet].location != NSNotFound)  {
        self.countLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.countLabel.textColor = [UIColor blackColor];
    
    if ([self.graduationYearTextField.text isEqual:@""] || [self.graduationYearTextField.text rangeOfCharacterFromSet:notNumberSet].location != NSNotFound) {
        self.graduationYearLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.graduationYearLabel.textColor = [UIColor blackColor];
    
    if (![[NSScanner scannerWithString:self.engineCapacityTextField.text] scanFloat:nil]) {//[self.engineCapacityTextField.text isEqual:@""] || [self.engineCapacityTextField.text rangeOfCharacterFromSet:notNumberSet].location != NSNotFound) {
        self.engineCapacityLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.engineCapacityLabel.textColor = [UIColor blackColor];
    
    if ([self.powerTextField.text isEqual:@""] || [self.powerTextField.text rangeOfCharacterFromSet:notNumberSet].location != NSNotFound) {
        self.powerLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.powerLabel.textColor = [UIColor blackColor];
    
    if ([self.priceTextField.text isEqual:@""] || [self.priceTextField.text rangeOfCharacterFromSet:notNumberSet].location != NSNotFound) {
        self.priceLabel.textColor = [UIColor redColor];
        flag = NO;
    } else
        self.priceLabel.textColor = [UIColor blackColor];
    
    if (!self.photosDict.count) {
        self.photosScrollView.layer.borderColor = [UIColor redColor].CGColor;
        flag = NO;
    } else
        self.photosScrollView.layer.borderColor = [UIColor blackColor].CGColor;
    
    return flag;
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    // define the block to call when we get the asset based on the url (below)
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        
        UIImage *image = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
        
        if(image) {
            
            [self.photosDict setObject:image forKey:[imageRep filename]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self configurePhotosScrollView];
            });
        }
    };
    
    // get the asset library and fetch the asset based on the ref url (pass in block above)
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Actions

- (void)actionClearFieldsButtonClicked:(UIBarButtonItem *)sender {
    
    self.brand = nil;
    self.bodyType = nil;
    self.kpp = nil;
    self.colour = nil;
    
    self.modelTextField.text = nil;
    self.graduationYearTextField.text = nil;
    self.engineCapacityTextField.text = nil;
    self.powerTextField.text = nil;
    self.priceTextField.text = nil;
    
    self.photosDict = [NSMutableDictionary dictionary];
    self.photoToDeleting = nil;
    [self configurePhotosScrollView];
    
    self.brandCell.detailTextLabel.text = @"не выбрана";
    self.bodytypeCell.detailTextLabel.text = @"не выбран";
    self.kppCell.detailTextLabel.text = @"не выбрана";
    self.colourCell.detailTextLabel.text = @"не выбран";
}

- (void)actionDeletePhoto:(UILongPressGestureRecognizer *)sender {
    
    if (sender.state != UIGestureRecognizerStateBegan)
        return;
    
    if ([sender.view isKindOfClass:[UIImageView class]]) {
        
        self.photoToDeleting = ((UIImageView *)sender.view).image;
        
        UIAlertView *deleteAlert = [[UIAlertView alloc]initWithTitle:@"Вы действительно хотите удалить выбранное фото?"
                                                             message:nil
                                                            delegate:self
                                                   cancelButtonTitle:@"Нет"
                                                   otherButtonTitles:@"Да", nil];
        deleteAlert.tag = 123;
        [deleteAlert show];
    }
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 123)
    {
        if (buttonIndex == 1 ) {
            
            for (NSString *key in self.photosDict.allKeys) {
                UIImage *image = [self.photosDict objectForKey:key];
                if ([image isEqual:self.photoToDeleting])
                {
                    [self.photosDict removeObjectForKey:key];
                    break;
                }
            }
            [self configurePhotosScrollView];
        } else {
            self.photoToDeleting = nil;
        }
    } else if (alertView.tag == 111)
    {
        Model *model = [NSEntityDescription insertNewObjectForEntityForName:@"Model"
                                                     inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
        model.brand = (Brand *)self.brand;
        model.name = self.modelTextField.text;
        model.bodyType = (BodyType *)self.bodyType;
        model.kpp = (KPP *)self.kpp;
        model.colour = (Colour *)self.colour;
        model.graduationYear = @([self.graduationYearTextField.text integerValue]);
        model.engineCapasity = @([self.engineCapacityTextField.text integerValue]);
        model.power = @([self.powerTextField.text integerValue]);
        model.count = @([self.countTextField.text integerValue]);
        model.price = @([self.priceTextField.text integerValue]);
        
        for (NSString *imageName in [self.photosDict allKeys]) {
            Photo *photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo"
                                                         inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
            
            [UIImage saveImage:[self.photosDict objectForKey:imageName] withName:imageName];
            photo.photoName = imageName;
            photo.model = model;
        }
        
        if ([CORE_DATA_MANAGER.managedObjectContext save:nil])
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}


#pragma mark - IBAction

- (IBAction)actionTakePhotoButtonClicked:(UIButton *)sender {
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self showImagePickerWithSoutceType:UIImagePickerControllerSourceTypeCamera];
    }
    else if(authStatus == AVAuthorizationStatusDenied)
    {
        [[[UIAlertView alloc]initWithTitle:@"У приложение нет доступа к камере"
                                   message:@"Вы можете активировать доступ в приватных настройках устройства"
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil] show];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        [[[UIAlertView alloc]initWithTitle:@"Что-то пошло не так"
                                   message:@"Попробуйте позже"
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil] show];
    }
    else if (authStatus == AVAuthorizationStatusNotDetermined)
    {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
                 [self showImagePickerWithSoutceType:UIImagePickerControllerSourceTypeCamera];
             else
                 [[[UIAlertView alloc]initWithTitle:@"У приложение нет доступа к камере"
                                            message:@"Вы можете активировать доступ в приватных настройках устройства"
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
         }];
    }
}

- (IBAction)actionChoosePhotoButtonClicked:(UIButton *)sender {
    
    [self showImagePickerWithSoutceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (IBAction)actionAddAutoButtonClicked:(UIButton *)sender {
    
    if ([self validateData])
    {
        UIAlertView *addAutoAlert = [[UIAlertView alloc]initWithTitle:@"Вы действительно ходите добавить сконфигурированный автомобиль?"
                                                              message:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                                    otherButtonTitles:@"Да", nil];
        addAutoAlert.tag = 111;
        [addAutoAlert show];
    }
}

@end
