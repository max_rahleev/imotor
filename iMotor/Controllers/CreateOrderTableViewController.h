//
//  CreateOrderTableViewController.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateOrderTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITableViewCell *brandCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *modelCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *bodytypeCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *colourCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *kppCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *graduationYearCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *engineCapasityCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *powerCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *priceCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *firstNameCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *lastNameCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *middleNameCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *emailCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *phoneCell;

@property (weak, nonatomic) IBOutlet UIButton *createOrderButton;

@property (strong, nonatomic) Model *model;

- (IBAction)actionCreateOrderButtonClicked:(UIButton *)sender;

@end
