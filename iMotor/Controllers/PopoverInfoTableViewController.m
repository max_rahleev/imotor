//
//  PopoverInfoTableViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 04.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "PopoverInfoTableViewController.h"

@interface PopoverInfoTableViewController ()
@property (strong, nonatomic) BaseModel *selectedModel;
@end

@implementation PopoverInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.navigationItemTitle;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                         target:self
                                                                                         action:@selector(actionAddNewItemButtonClicked:)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                          target:self
                                                                                          action:@selector(actionDoneButtonClicked:)];
}


#pragma mark - Seters

- (void)setContentArray:(NSArray *)contentArray {
    
    _contentArray = contentArray;
    
    [self.tableView reloadData];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.contentArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InfoCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    BaseModel *model = (BaseModel *)[self.contentArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = model.name;
    if (![model isEqual:self.selectedModel])
        cell.accessoryType = UITableViewCellAccessoryNone;
    else
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return self.sectionTitle;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.selectedModel = [self.contentArray objectAtIndex:indexPath.row];
    
    for (int i = 0; i < [tableView numberOfRowsInSection:0]; i++) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
        [tableView cellForRowAtIndexPath:path].accessoryType = UITableViewCellAccessoryNone;
    }
    
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Actions

- (void)actionDoneButtonClicked:(UIBarButtonItem *)sender {
    
    if (self.selectedModel && self.didSelectBlock)
        self.didSelectBlock(self.selectedModel);
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionAddNewItemButtonClicked:(UIBarButtonItem *)sender {
    
    UIAlertView *addItemAlert = [[UIAlertView alloc]initWithTitle:self.addNewItemTitle
                                                          message:nil
                                                         delegate:self
                                                cancelButtonTitle:@"Отмена"
                                                otherButtonTitles:@"Добавить", nil];
    
    addItemAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [addItemAlert textFieldAtIndex:0].autocapitalizationType = UITextAutocapitalizationTypeWords;
    
    [addItemAlert show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1)
    {
        UITextField *nameTF = [alertView textFieldAtIndex:0];
        
        if ([nameTF.text isEqual:@""])
            nameTF.layer.borderColor = [UIColor redColor].CGColor;
        else
            if (self.addNewItemBlock)
                self.addNewItemBlock(nameTF.text);
    }
}

@end
