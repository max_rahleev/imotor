//
//  SignupEmployeeTableViewController.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoTableViewCell.h"

@interface SignupEmployeeTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet InfoTableViewCell  *firstNameCell;
@property (weak, nonatomic) IBOutlet InfoTableViewCell  *lastNameCell;
@property (weak, nonatomic) IBOutlet InfoTableViewCell  *middleNameCell;
@property (weak, nonatomic) IBOutlet InfoTableViewCell  *yearOfBidthcell;
@property (weak, nonatomic) IBOutlet InfoTableViewCell  *passportNumberCell;
@property (weak, nonatomic) IBOutlet InfoTableViewCell  *phoneCell;
@property (weak, nonatomic) IBOutlet InfoTableViewCell  *emailCell;
@property (weak, nonatomic) IBOutlet InfoTableViewCell  *loginCell;
@property (weak, nonatomic) IBOutlet InfoTableViewCell  *passwordCell;
@property (weak, nonatomic) IBOutlet InfoTableViewCell  *passwordRepeatCell;
@property (weak, nonatomic) IBOutlet UISegmentedControl *userTypeSegmentedControl;


@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;

@property (weak, nonatomic) IBOutlet UIButton *signUpButton;

- (IBAction)actionSignUpEmployeeButtonClicked:(UIButton *)sender;

@end
