//
//  ProfileTableViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "ProfileTableViewController.h"

@interface ProfileTableViewController ()
@property (strong, nonatomic) UIDatePicker *datePicker;
@end

@implementation ProfileTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Профиль";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Очистить" style:UIBarButtonItemStylePlain target:self action:@selector(actionClearFieldsButtonClicked:)];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Выйти" style:UIBarButtonItemStylePlain target:self action:@selector(actionLogoutButtonClicked:)];
    
    self.datePicker = [[UIDatePicker alloc]init];
    self.datePicker.date = COMMON.currentUser.dateOfBirth;
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    [self.datePicker addTarget:self action:@selector(actionDataPickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.yearOfBidthcell.infoTextField setInputView:self.datePicker];
    
    //'Done' btn for TF
    UIToolbar* phoneKeyboardDoneButtonView = [[UIToolbar alloc] init];
    [phoneKeyboardDoneButtonView sizeToFit];
    UIBarButtonItem *flexiblSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:nil
                                                                                  action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self.view
                                                                  action:@selector(endEditing:)];
    [phoneKeyboardDoneButtonView setItems:@[flexiblSpace, doneButton]];
    self.yearOfBidthcell.infoTextField.inputAccessoryView = phoneKeyboardDoneButtonView;
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self.view action:@selector(endEditing:)]];
    
    [self configureTable];
}

#pragma mark - Helpers

- (void)configureTable {
    
    User *user = COMMON.currentUser;
    
    self.firstNameCell.infoTextField.text      = user.firstName;
    self.lastNameCell.infoTextField.text       = user.lastName;
    self.middleNameCell.infoTextField.text     = user.middleName;
    self.phoneCell.infoTextField.text          = user.phone;
    self.passportNumberCell.infoTextField.text = user.passportNumber;
    self.emailCell.infoTextField.text          = user.email;
    self.loginCell.infoTextField.text          = user.login;
    self.passwordCell.infoTextField.text       = user.password;
    self.passwordRepeatCell.infoTextField.text = user.password;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"d MMMM yyyy'г.'"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    self.yearOfBidthcell.infoTextField.text = [dateFormatter stringFromDate:user.dateOfBirth];
}

- (BOOL)validateData {
    
    BOOL flag = YES;
    
    for (UITextField *tf in self.textFields)
    {
        if ([tf.text isEqualToString:@""])
        {
            [self nonValidTextField:tf];
            flag = NO;
        } else
            [self validTextField:tf];
    }
    
    if (![COMMON validateEmail:self.emailCell.infoTextField.text]) {
        [self nonValidTextField:self.emailCell.infoTextField];
        flag = NO;
    } else {
        [self validTextField:self.emailCell.infoTextField];
    }
    
    NSString *pass = self.passwordCell.infoTextField.text;
    NSString *repeatPass = self.passwordRepeatCell.infoTextField.text;
    
    if ([pass isEqualToString:repeatPass] && ![pass isEqualToString:@""] && ![repeatPass isEqualToString:@""] && pass.length >= 6) {
        [self validTextField:self.passwordCell.infoTextField];
        [self validTextField:self.passwordRepeatCell.infoTextField];
    } else {
        [self nonValidTextField:self.passwordCell.infoTextField];
        [self nonValidTextField:self.passwordRepeatCell.infoTextField];
        flag = NO;
    }
    
    return flag;
}

- (void)validTextField:(UITextField *)textField {
    
    textField.layer.borderColor = [UIColor clearColor].CGColor;
}

- (void)nonValidTextField:(UITextField *)textField {
    
    textField.layer.borderWidth = 0.5f;
    textField.layer.borderColor = [UIColor redColor].CGColor;
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 123)
    {
        if (buttonIndex == 1)
        {
            User *user          = COMMON.currentUser;
            
            user.firstName      = [self.firstNameCell.infoTextField.text capitalizedString];
            user.lastName       = [self.lastNameCell.infoTextField.text capitalizedString];
            user.middleName     = [self.middleNameCell.infoTextField.text capitalizedString];
            user.email          = self.emailCell.infoTextField.text;
            user.login          = self.loginCell.infoTextField.text;
            user.phone          = self.phoneCell.infoTextField.text;
            user.dateOfBirth    = self.datePicker.date;
            user.passportNumber = self.passportNumberCell.infoTextField.text;
            user.password       = self.passwordCell.infoTextField.text;
            user.type           = [CORE_DATA_MANAGER userTypeWithType:UserRoleClient];
            
            if ([CORE_DATA_MANAGER.managedObjectContext save:nil]) {
                COMMON.currentUser = user;
                
                UIAlertView *signupSuccessAlert = [[UIAlertView alloc]initWithTitle:@"Изменения сохранены"
                                                                            message:nil
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil];
                signupSuccessAlert.tag = 321;
                [signupSuccessAlert show];
            }
        }
    } else if (alertView.tag == 321) {
        [self configureTable];
    }
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger index = [self.textFields indexOfObject:textField];
    if (index < self.textFields.count - 1)
        [[self.textFields objectAtIndex:index + 1]becomeFirstResponder];
    else
        [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Actions

- (void)actionClearFieldsButtonClicked:(UIBarButtonItem *)sender {
    
    for (UITextField *tf in self.textFields)
        tf.text = nil;
}

- (void)actionDataPickerValueChanged:(UIDatePicker *)sender {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"d MMMM yyyy'г.'"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    
    self.yearOfBidthcell.infoTextField.text = [dateFormatter stringFromDate:sender.date];
}

- (void)actionLogoutButtonClicked:(UIBarButtonItem *)sender {
    
    COMMON.currentUser = nil;
    COMMON.userLoggedIn = NO;
    [COMMON.leftMenuTableViewController.tableView reloadData];
    [COMMON.leftMenuTableViewController.tableView.delegate tableView:COMMON.leftMenuTableViewController.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

#pragma mark - IBActions

- (IBAction)actionSaveChangesButtonClicked:(UIButton *)sender {
    
    if ([self validateData])
    {
        UIAlertView *signUpAlert = [[UIAlertView alloc]initWithTitle:@"Изменить личные данные?"
                                                             message:nil
                                                            delegate:self
                                                   cancelButtonTitle:@"Отмена"
                                                   otherButtonTitles:@"Да", nil];
        
        signUpAlert.tag = 123;
        [signUpAlert show];
    }
}

@end
