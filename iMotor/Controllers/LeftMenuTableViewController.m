//
//  LeftMenuTableViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 04.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "LeftMenuTableViewController.h"

@interface LeftMenuTableViewController ()
@property (strong, nonatomic) NSArray *menuLoginAsAdmin;
@property (strong, nonatomic) NSArray *detailVCIDsLoginAsAdmin;

@property (strong, nonatomic) NSArray *menuLoginAsManager;
@property (strong, nonatomic) NSArray *detailVCIDsLoginAsManager;

@property (strong, nonatomic) NSArray *menuLoginAsClient;
@property (strong, nonatomic) NSArray *detailVCIDsLoginAsClient;

@property (strong, nonatomic) NSArray *menuNotLoggedIn;
@property (strong, nonatomic) NSArray *detailVCIDsNotLoggedIn;

@property (strong, nonatomic) UITableViewCell*selectedCell;

@end

@implementation LeftMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.menuLoginAsAdmin = @[@"Автомобили в наличии",
                              @"Заказы на автомобили",
                              @"Управление персоналом",
                              @"Профиль",
                              @"Отчетность",
                              @"Информация"];
    
    self.detailVCIDsLoginAsAdmin = @[@"CarsCollectionViewController",
                                     @"SalonOrdersTableViewController",
                                     @"EmployeesTableViewController",
                                     @"ProfileTableViewController",
                                     @"AddNewCarTableTableViewController",
                                     @"AddNewCarTableTableViewController"];
    
    self.menuLoginAsClient = @[@"Автомобили в наличии",
                               @"Мои заказы",
                               @"Профиль",
                               @"Информация"];
    
    self.detailVCIDsLoginAsClient = @[@"CarsCollectionViewController",
                                      @"ClientOrdersTableViewController",
                                      @"ProfileTableViewController",
                                      @"AddNewCarTableTableViewController"];
    
    self.menuNotLoggedIn = @[@"Автомобили в наличии",
                             @"Вход",
                             @"Информация"];
    
    self.detailVCIDsNotLoggedIn = @[@"CarsCollectionViewController",
                                    @"LoginViewController",
                                    @"AddNewCarTableTableViewController"];
    
    COMMON.leftMenuTableViewController = self;
    
    [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (!IS_USER_LOGGED_IN)
        return self.menuNotLoggedIn.count;
    else if (IS_USER_LOGGED_IN && IS_CLIENT)
        return self.menuLoginAsClient.count;
    else
        return self.menuLoginAsAdmin.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell"];
    
    if (!cell)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MenuCell"];
    
    if (!IS_USER_LOGGED_IN)
        cell.textLabel.text = [self.menuNotLoggedIn objectAtIndex:indexPath.row];
    else if (IS_USER_LOGGED_IN && IS_CLIENT)
        cell.textLabel.text = [self.menuLoginAsClient objectAtIndex:indexPath.row];
    else
        cell.textLabel.text = [self.menuLoginAsAdmin objectAtIndex:indexPath.row];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.selectedCell isEqual:[tableView cellForRowAtIndexPath:indexPath]])
        return;
    
    self.selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    
    for (int i = 0; i < [tableView numberOfRowsInSection:0]; i++) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:path];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
    }
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor colorWithRed:12/255.f green:132/255.f blue:200/255.f alpha:1.f];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    [self.tableView layoutSubviews];
    
    NSString *vcName = nil;
    if (!IS_USER_LOGGED_IN)
        vcName = [self.detailVCIDsNotLoggedIn objectAtIndex:indexPath.row];
    else if (IS_USER_LOGGED_IN && IS_CLIENT)
        vcName = [self.detailVCIDsLoginAsClient objectAtIndex:indexPath.row];
    else
        vcName = [self.detailVCIDsLoginAsAdmin objectAtIndex:indexPath.row];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:vcName];
    UIViewController *newDetailVC = [[UINavigationController alloc]initWithRootViewController:vc];
    
    NSArray *newVCs = @[COMMON.splitViewController.viewControllers.firstObject, newDetailVC];
    COMMON.splitViewController.viewControllers = newVCs;
}


@end
