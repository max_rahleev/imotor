//
//  SalonOrdersTableViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 06.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "SalonOrdersTableViewController.h"
#import "SalonOrderTableViewCell.h"

@interface SalonOrdersTableViewController ()
@property (strong, nonatomic) NSArray *orders;
@end

@implementation SalonOrdersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadOrders];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.orders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SalonOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SalonOrderTableViewCell"];
    
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"SalonOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"SalonOrderTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"SalonOrderTableViewCell"];
    }
    
    Order *order = [self.orders objectAtIndex:indexPath.row];
    [cell configureCellWithOrder:order];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 150.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.1f;
}


#pragma mark - Load Data

- (void)loadOrders {
    
    NSFetchRequest *reques = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:orderTableName inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
    [reques setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    [reques setSortDescriptors:@[sortDescriptor]];
    
    if ([CORE_DATA_MANAGER.managedObjectContext countForFetchRequest:reques error:nil])
    {
        self.orders = [CORE_DATA_MANAGER.managedObjectContext executeFetchRequest:reques error:nil];
    } else {
        [[[UIAlertView alloc]initWithTitle:@"Заказов нет"
                                  message:nil
                                 delegate:nil
                        cancelButtonTitle:@"OK"
                        otherButtonTitles:nil] show];
    }
    
}


@end
