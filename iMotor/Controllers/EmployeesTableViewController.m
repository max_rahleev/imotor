//
//  EmployeesTableViewController.m
//  iMotor
//
//  Created by Maksim Rakhleev on 06.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "EmployeesTableViewController.h"
#import "EmployeeTableViewCell.h"

@interface EmployeesTableViewController ()
@property (strong, nonatomic) NSArray *employees;
@end

@implementation EmployeesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Список сотрудников";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                              target:self
                                              action:@selector(actionAddNewEmployeeButtonClicked:)];
    
    [self loadEmployees];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadEmployees];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.employees.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EmployeeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmployeeTableViewCell"];
    
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"EmployeeTableViewCell" bundle:nil] forCellReuseIdentifier:@"EmployeeTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"EmployeeTableViewCell"];
    }
    
    User *user = [self.employees objectAtIndex:indexPath.row];
    [cell configureCellWithUser:user];
    cell.updateTableBlock = ^() {
        [self loadEmployees];
    };
    
    if (indexPath.row % 2)
        cell.backgroundColor = [UIColor colorWithRed:216/255.f green:216/255.f blue:216/255.f alpha:1.f];
    else
        cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 115.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.1f;
}


#pragma mark - Actions

- (void)actionAddNewEmployeeButtonClicked:(UIBarButtonItem *)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *createNewEmployeeVC = [storyboard instantiateViewControllerWithIdentifier:@"SignupEmployeeTableViewController"];
    [self.navigationController pushViewController:createNewEmployeeVC animated:YES];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(alertView.tag == 123 && buttonIndex == 1)
    {
        [self actionAddNewEmployeeButtonClicked:nil];
    }
}


#pragma mark - Load Data

- (void)loadEmployees {
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:userTableName inManagedObjectContext:CORE_DATA_MANAGER.managedObjectContext];
    [request setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type.type == %@ OR type.type == %@", @(UserRoleAdmin), @(UserRoleManager)];
    [request setPredicate:predicate];
    
    NSSortDescriptor *firstNameSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES];
    NSSortDescriptor *lastNameSortDescriptor  = [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES];
    [request setSortDescriptors:@[firstNameSortDescriptor, lastNameSortDescriptor]];
    
    if ([CORE_DATA_MANAGER.managedObjectContext countForFetchRequest:request error:nil])
    {
        self.employees = [CORE_DATA_MANAGER.managedObjectContext executeFetchRequest:request error:nil];
        [self.tableView reloadData];
    } else {
        UIAlertView *addNewAlert = [[UIAlertView alloc]initWithTitle:@"В салоне кроме Вас еще нет сотрудников"
                                                             message:@"Добавить?"
                                                            delegate:self
                                                   cancelButtonTitle:@"Отмена"
                                                   otherButtonTitles:@"Да", nil];
        addNewAlert.tag = 123;
        [addNewAlert show];
    }
}
















@end
