//
//  CoreDataManager.h
//  iMotor
//
//  Created by Maksim Rakhleev on 04.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Common.h"

static NSString *brandTableName       = @"Brand";
static NSString *colourTableName      = @"Colour";
static NSString *kppTableName         = @"KPP";
static NSString *orderStatusTableName = @"OrderStatus";
static NSString *bodyTypeTableName    = @"BodyType";
static NSString *photoTableName       = @"Photo";
static NSString *userTypeTableName    = @"UserType";
static NSString *userTableName        = @"User";
static NSString *orderTableName       = @"Order";
static NSString *modelTableName       = @"Model";

@class BaseModel, Model, UserType, OrderStatus;

@interface CoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (CoreDataManager *)sharedManager;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


//only subclasses of BaseModel
- (NSArray *)readAllDataFromTableWithName:(NSString *)tableName;
- (BaseModel *)addItemWithName:(NSString *)name inTableWithTitle:(NSString *)tableName;

- (NSArray *)readModelsForBrand:(BaseModel *)brand;
- (void)addModel:(Model *)model;

- (void)printAllModels;

- (NSInteger)readAllUserTypes;
- (void)setUsersTypes;
- (UserType *)userTypeWithType:(UserRole)type;

- (NSInteger)readAllOrderStatuses;
- (void)setOrderStatuses;
- (OrderStatus *)orderStatusWithStatus:(StatusOfOrder)status;


@end
