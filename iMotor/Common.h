//
//  Common.h
//  iMotor
//
//  Created by Maksim Rakhleev on 04.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    UserRoleAdmin,
    UserRoleManager,
    UserRoleClient
} UserRole;

typedef enum {
    StatusOfOrderOrdered,
    StatusOfOrderAccepter,
    StatusOfOrderDeclined,
    StatusOfOrderDelivered
} StatusOfOrder;

@class User;

@interface Common : NSObject

+ (Common *)sharedCommon;

- (BOOL)validateEmail:(NSString *)email;

@property (strong, nonatomic) UISplitViewController *splitViewController;
@property (strong, nonatomic) UITableViewController *leftMenuTableViewController;

@property (strong, nonatomic) User *currentUser;
@property (assign, nonatomic, getter = isUserLoggedIn) BOOL userLoggedIn;

@end
