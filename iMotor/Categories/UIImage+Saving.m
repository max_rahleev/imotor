//
//  UIImage+Saving.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "UIImage+Saving.h"
#import "UIImage+Resize.h"

@implementation UIImage (Saving)

+ (void)saveImage:(UIImage *)image withName:(NSString *)name {
    
    NSString *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@", name]];
    
    CGFloat aspectRatio = image.size.width / image.size.height;
    UIImage *scaledImage = [image scaledImageToSize:CGSizeMake(MAX(SCREEN_HEIGHT, SCREEN_WIDTH), MAX(SCREEN_WIDTH, SCREEN_HEIGHT) / aspectRatio)];
    
    [UIImagePNGRepresentation(scaledImage) writeToFile:imagePath atomically:YES];
}

+ (UIImage *)loadImageWithName:(NSString *)name {
    
    NSString *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@", name]];
    
    return [UIImage imageWithContentsOfFile:imagePath];
}

@end
