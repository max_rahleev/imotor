//
//  UIImage+Resize.m
//  Frizo
//
//  Created by Maksim Rakhleev on 27.01.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "UIImage+Resize.h"

@implementation UIImage (Resize)

- (UIImage *)scaledImageToSize:(CGSize)newSize;
{
#warning MEMORY WARNING MAYBE HERE
    UIGraphicsBeginImageContextWithOptions(newSize, NO, [UIScreen mainScreen].scale);
    [self drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
