//
//  UIImage+Resize.h
//  Frizo
//
//  Created by Maksim Rakhleev on 27.01.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

- (UIImage *)scaledImageToSize:(CGSize)newSize;


@end
