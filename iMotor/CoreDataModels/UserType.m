//
//  UserType.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "UserType.h"
#import "User.h"


@implementation UserType

@dynamic type;
@dynamic users;

@end
