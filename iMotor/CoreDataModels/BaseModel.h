//
//  BaseModel.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BaseModel : NSManagedObject

@property (nonatomic, retain) NSString * name;

@end
