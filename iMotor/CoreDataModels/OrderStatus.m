//
//  OrderStatus.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "OrderStatus.h"
#import "Order.h"


@implementation OrderStatus

@dynamic status;
@dynamic orders;

@end
