//
//  User.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Order, UserType;

@interface User : NSManagedObject

@property (nonatomic, retain) NSDate * dateOfBirth;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * login;
@property (nonatomic, retain) NSString * middleName;
@property (nonatomic, retain) NSString * passportNumber;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSSet *orders;
@property (nonatomic, retain) UserType *type;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addOrdersObject:(Order *)value;
- (void)removeOrdersObject:(Order *)value;
- (void)addOrders:(NSSet *)values;
- (void)removeOrders:(NSSet *)values;

@end
