//
//  Order.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Model, OrderStatus, User;

@interface Order : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) User *client;
@property (nonatomic, retain) Model *model;
@property (nonatomic, retain) OrderStatus *status;

@end
