//
//  Photo.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Model;

@interface Photo : NSManagedObject

@property (nonatomic, retain) NSString * photoName;
@property (nonatomic, retain) Model *model;

@end
