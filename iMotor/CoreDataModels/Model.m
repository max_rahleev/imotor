//
//  Model.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "Model.h"
#import "BodyType.h"
#import "Brand.h"
#import "Colour.h"
#import "KPP.h"
#import "Order.h"
#import "Photo.h"


@implementation Model

@dynamic engineCapasity;
@dynamic graduationYear;
@dynamic power;
@dynamic count;
@dynamic price;
@dynamic bodyType;
@dynamic brand;
@dynamic colour;
@dynamic kpp;
@dynamic orders;
@dynamic photos;

@end
