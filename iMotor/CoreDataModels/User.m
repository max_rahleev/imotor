//
//  User.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "User.h"
#import "Order.h"
#import "UserType.h"


@implementation User

@dynamic dateOfBirth;
@dynamic email;
@dynamic firstName;
@dynamic lastName;
@dynamic login;
@dynamic middleName;
@dynamic passportNumber;
@dynamic password;
@dynamic phone;
@dynamic orders;
@dynamic type;

@end
