//
//  Model.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseModel.h"

@class BodyType, Brand, Colour, KPP, Order, Photo;

@interface Model : BaseModel

@property (nonatomic, retain) NSNumber * engineCapasity;
@property (nonatomic, retain) NSNumber * graduationYear;
@property (nonatomic, retain) NSNumber * power;
@property (nonatomic, retain) NSNumber * count;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) BodyType *bodyType;
@property (nonatomic, retain) Brand *brand;
@property (nonatomic, retain) Colour *colour;
@property (nonatomic, retain) KPP *kpp;
@property (nonatomic, retain) NSSet *orders;
@property (nonatomic, retain) NSSet *photos;
@end

@interface Model (CoreDataGeneratedAccessors)

- (void)addOrdersObject:(Order *)value;
- (void)removeOrdersObject:(Order *)value;
- (void)addOrders:(NSSet *)values;
- (void)removeOrders:(NSSet *)values;

- (void)addPhotosObject:(Photo *)value;
- (void)removePhotosObject:(Photo *)value;
- (void)addPhotos:(NSSet *)values;
- (void)removePhotos:(NSSet *)values;

@end
