//
//  Photo.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "Photo.h"
#import "Model.h"


@implementation Photo

@dynamic photoName;
@dynamic model;

@end
