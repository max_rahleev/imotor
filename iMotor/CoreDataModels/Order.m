//
//  Order.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "Order.h"
#import "Model.h"
#import "OrderStatus.h"
#import "User.h"


@implementation Order

@dynamic date;
@dynamic client;
@dynamic model;
@dynamic status;

@end
