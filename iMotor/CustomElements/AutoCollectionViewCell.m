//
//  AutoCollectionViewCell.m
//  iMotor
//
//  Created by Maksim Rakhleev on 04.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "AutoCollectionViewCell.h"
#import "UIImage+Saving.h"
#import "CreateOrderTableViewController.h"

@interface AutoCollectionViewCell () <UIScrollViewDelegate>

@end

@implementation AutoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.hidden = YES;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    self.createOrderButton.titleLabel.numberOfLines = 1;
    self.createOrderButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.createOrderButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.createOrderButton.titleLabel.lineBreakMode = NSLineBreakByClipping;
}

- (void)configureCellWithModel:(Model *)model {
    
    _model = model;
    
    [self layoutIfNeeded];
    
    self.brandLabel.text = model.brand.name;
    self.modelLabel.text = model.name;
    
    self.infoTextView.text = [NSString stringWithFormat:@"%@, %@ цвет, объем двигателя - %@см3, мощность - %@л.с., %@ КПП",
                              [model.bodyType.name capitalizedString], [model.colour.name lowercaseString], model.engineCapasity, model.power,  [model.kpp.name lowercaseString]];
    
    if ([model.count integerValue] == 0) {
        self.createOrderButton.enabled = NO;
        self.createOrderButton.alpha = 0.5f;
    }
    
    if (IS_USER_LOGGED_IN && IS_CLIENT) {
        [self.createOrderButton setTitle:[NSString stringWithFormat:@"Заказать за %@$", model.price] forState:UIControlStateNormal];
        self.priceLabel.hidden = YES;
    } else {
        self.priceLabel.text = [NSString stringWithFormat:@"%@$", model.price];
        self.createOrderButton.hidden = YES;
    }
    
    [self configurePhotosScrollView];
    
    self.hidden = NO;
}

- (void)configurePhotosScrollView {
    
    self.photosScrollView.delegate = self;
    
    for (UIView *view in self.photosScrollView.subviews)
        [view removeFromSuperview];
    
    CGSize scrollContentSize = CGSizeZero;
    scrollContentSize.height = CGRectGetHeight(self.photosScrollView.bounds);
    self.photosScrollView.contentSize = scrollContentSize;
    
    self.pageControl.numberOfPages = self.model.photos.count;
    self.pageControl.currentPage = 0;
    
    for (Photo *photo in self.model.photos) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.photosScrollView.contentSize.width, 0, CGRectGetWidth(self.photosScrollView.bounds), CGRectGetHeight(self.photosScrollView.bounds))];
        imageView.contentMode = UIViewContentModeScaleAspectFit;

        UIImage *image = [UIImage loadImageWithName:photo.photoName];
        imageView.image = image;
        
        [self.photosScrollView addSubview:imageView];
        
        scrollContentSize = self.photosScrollView.contentSize;
        scrollContentSize.width += CGRectGetWidth(self.photosScrollView.bounds);
        self.photosScrollView.contentSize = scrollContentSize;
    }
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = CGRectGetWidth(scrollView.bounds);
    CGFloat fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
}


#pragma mark - IBActions

- (IBAction)actionCreateOrderButtonClicked:(UIButton *)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateOrderTableViewController *createOrderVC = [storyboard instantiateViewControllerWithIdentifier:@"CreateOrderTableViewController"];
    createOrderVC.model = self.model;
    [self.parentViewController.navigationController pushViewController:createOrderVC animated:YES];
}

@end
