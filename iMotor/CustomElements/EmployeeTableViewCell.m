//
//  EmployeeTableViewCell.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "EmployeeTableViewCell.h"

@implementation EmployeeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.hidden = YES;
}

- (void)configureCellWithUser:(User *)user {
    
    _user = user;
    
    self.deleteUserButton.hidden = NO;
    
    self.fioAndTypeLabel.text = [NSString stringWithFormat:@"%@ %@ %@, %@", user.firstName, user.lastName, user.middleName, [USER_ROLES objectAtIndex:[user.type.type integerValue]]];
    self.emailLabel.text = [NSString stringWithFormat:@"E-mail: %@", user.email];
    self.phoneLabel.text = [NSString stringWithFormat:@"Тел.: %@", user.phone];
    
    if ([user.type.type integerValue] == UserRoleAdmin)
        self.deleteUserButton.hidden = YES;
    else
        self.deleteUserButton.hidden = NO;
    
    self.hidden = NO;
}


#pragma makr - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 123 && buttonIndex == 1)
    {
        [CORE_DATA_MANAGER.managedObjectContext deleteObject:self.user];
        if ([CORE_DATA_MANAGER.managedObjectContext save:nil])
        {
            UIAlertView *deleteAlert = [[UIAlertView alloc]initWithTitle:@"Пользователь успешно удален"
                                                                 message:nil
                                                                delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
            deleteAlert.tag = 111;
            [deleteAlert show];
        }
    } else if (alertView.tag == 111)
    {
        if (self.updateTableBlock)
            self.updateTableBlock();
    }
}


#pragma mark - IBActions

- (IBAction)actionDeleteUserFromDBButtonClicked:(UIButton *)sender {
    
    UIAlertView *deleteAlert = [[UIAlertView alloc]initWithTitle:@"Вы действительно хотите удалить пользователя?"
                                                         message:@"Учетная запись будет удалена полностью без возможности восстановления"
                                                        delegate:self
                                               cancelButtonTitle:@"Отмена"
                                               otherButtonTitles:@"Да", nil];
    deleteAlert.tag = 123;
    [deleteAlert show];
}

@end
