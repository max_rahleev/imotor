//
//  SalonOrderTableViewCell.h
//  iMotor
//
//  Created by Maksim Rakhleev on 06.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalonOrderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel     *carLabel;
@property (weak, nonatomic) IBOutlet UILabel     *clientLabel;
@property (weak, nonatomic) IBOutlet UILabel     *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton    *acceptButton;
@property (weak, nonatomic) IBOutlet UIButton    *declineButton;
@property (weak, nonatomic) IBOutlet UIButton    *deliveredButton;

- (IBAction)actionAcceptOrderButtonClicked:(UIButton *)sender;
- (IBAction)actionDeclineOrderButtonClicked:(UIButton *)sender;
- (IBAction)actiondeliveredOrderButtonClicked:(UIButton *)sender;

@property (strong, nonatomic) Order *order;

- (void)configureCellWithOrder:(Order *)order;

@end
