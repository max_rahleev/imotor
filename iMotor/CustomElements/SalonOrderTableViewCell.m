//
//  SalonOrderTableViewCell.m
//  iMotor
//
//  Created by Maksim Rakhleev on 06.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "SalonOrderTableViewCell.h"
#import "UIImage+Saving.h"

@implementation SalonOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.hidden = YES;
}

- (void)configureCellWithOrder:(Order *)order {
    
    _order = order;
    
    self.carLabel.text = [NSString stringWithFormat:@"%@ %@, %@, двигатель - %@см3, мощность двигателя - %@л.с., год выпуска - %@", order.model.brand.name, order.model.name, order.model.bodyType.name, order.model.engineCapasity, order.model.power, order.model.graduationYear];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"d MMMM yyyy'г.'"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    self.clientLabel.text = [NSString stringWithFormat:@"%@ %@ %@, тел.: %@, e-mail: %@. Дата заказа: %@", order.client.lastName, order.client.firstName, order.client.middleName, order.client.phone, order.client.email, [dateFormatter stringFromDate:order.date]];
    
    self.photoImageView.image = [UIImage loadImageWithName:((Photo *)[order.model.photos allObjects].firstObject).photoName];
    
    
    self.priceLabel.text = [NSString stringWithFormat:@"%@$", order.model.price];
    
    StatusOfOrder status = (StatusOfOrder)[order.status.status integerValue];
    
    self.backgroundColor = [UIColor whiteColor];
    self.acceptButton.hidden = NO;
    self.declineButton.hidden = NO;
    self.deliveredButton.hidden = NO;
    
    if (status == StatusOfOrderOrdered)
    {
        self.backgroundColor = [UIColor colorWithRed:244/255.f green:221/255.f blue:118/255.f alpha:0.2f];
    } else if (status == StatusOfOrderAccepter)
    {
        self.acceptButton.hidden = YES;
        self.declineButton.hidden = YES;
        self.backgroundColor = [UIColor colorWithRed:26/255.f green:210/255.f blue:118/255.f alpha:0.2f];
    } else if (status == StatusOfOrderDeclined)
    {
        self.acceptButton.hidden = YES;
        self.declineButton.hidden = YES;
        self.deliveredButton.hidden = YES;
        self.backgroundColor = [UIColor colorWithRed:224/255.f green:32/255.f blue:33/255.f alpha:0.2f];
    } else if (status == StatusOfOrderDelivered)
    {
        self.acceptButton.hidden = YES;
        self.declineButton.hidden = YES;
        self.deliveredButton.hidden = YES;
        self.backgroundColor = [UIColor colorWithRed:0/255.f green:128/255.f blue:255/255.f alpha:0.2f];
    }
    
    self.hidden = NO;
}


#pragma mark - IBActions

- (IBAction)actionAcceptOrderButtonClicked:(UIButton *)sender {
    
    self.order.status = [CORE_DATA_MANAGER orderStatusWithStatus:StatusOfOrderAccepter];
    if ([CORE_DATA_MANAGER.managedObjectContext save:nil])
        [self configureCellWithOrder:self.order];
}

- (IBAction)actionDeclineOrderButtonClicked:(UIButton *)sender {
    
    self.order.status = [CORE_DATA_MANAGER orderStatusWithStatus:StatusOfOrderDeclined];
    if ([CORE_DATA_MANAGER.managedObjectContext save:nil])
        [self configureCellWithOrder:self.order];
}

- (IBAction)actiondeliveredOrderButtonClicked:(UIButton *)sender {
    
    self.order.status = [CORE_DATA_MANAGER orderStatusWithStatus:StatusOfOrderDelivered];
    if (self.order.model.count > 0)
        self.order.model.count = @([self.order.model.count integerValue] - 1);
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"Автомобиля нет в наличии"
                                  message:nil
                                 delegate:nil
                        cancelButtonTitle:@"OK"
                        otherButtonTitles:nil] show];
    }
    if ([CORE_DATA_MANAGER.managedObjectContext save:nil])
        [self configureCellWithOrder:self.order];
}

@end
