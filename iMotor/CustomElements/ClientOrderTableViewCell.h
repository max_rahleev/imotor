//
//  ClientOrderTableViewCell.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClientOrderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel     *brandAndModelLabel;
@property (weak, nonatomic) IBOutlet UILabel     *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel     *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel     *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton    *declineOrderButton;

- (IBAction)actionDeclineOrderButtonClicked:(UIButton *)sender;

@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) UIViewController *parentViewController;

- (void)configureCellWithOrder:(Order *)order;


@end
