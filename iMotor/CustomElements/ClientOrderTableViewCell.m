//
//  ClientOrderTableViewCell.m
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import "ClientOrderTableViewCell.h"
#import "UIImage+Saving.h"

@implementation ClientOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.hidden = YES;
}

- (void)configureCellWithOrder:(Order *)order {
    
    _order = order;
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self layoutIfNeeded];
    
    self.brandAndModelLabel.text = [NSString stringWithFormat:@"%@ %@", order.model.brand.name, order.model.name];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy'г.'"];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    self.dateLabel.text = [NSString stringWithFormat:@"Дата заказа: %@", [dateFormatter stringFromDate:order.date]];
    self.statusLabel.text = [NSString stringWithFormat:@"Статус заказа: %@", [(NSString *)[ORDER_STATUSES objectAtIndex:[order.status.status integerValue]] lowercaseString]];
    self.priceLabel.text = [NSString stringWithFormat:@"%@$", order.model.price];
    
    NSString *firstImageName = ((Photo *)[order.model.photos allObjects].firstObject).photoName;
    self.photoImageView.image = [UIImage loadImageWithName:firstImageName];
    
    StatusOfOrder status = (StatusOfOrder)[order.status.status integerValue];
    if (status == StatusOfOrderDeclined || status == StatusOfOrderDelivered)
        self.declineOrderButton.hidden = YES;
    else
        self.declineOrderButton.hidden = NO;
    
    if (status == StatusOfOrderOrdered)
        self.backgroundColor = [UIColor colorWithRed:244/255.f green:221/255.f blue:118/255.f alpha:0.2f];
    else if (status == StatusOfOrderAccepter)
        self.backgroundColor = [UIColor colorWithRed:26/255.f green:210/255.f blue:118/255.f alpha:0.2f];
    else if (status == StatusOfOrderDeclined)
        self.backgroundColor = [UIColor colorWithRed:224/255.f green:32/255.f blue:33/255.f alpha:0.2f];
    else if (status == StatusOfOrderDelivered)
        self.backgroundColor = [UIColor colorWithRed:0/255.f green:128/255.f blue:255/255.f alpha:0.2f];
    
    self.hidden = NO;
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 123 && buttonIndex == 1)
    {
        self.order.status = [CORE_DATA_MANAGER orderStatusWithStatus:StatusOfOrderDeclined];
        if([CORE_DATA_MANAGER.managedObjectContext save:nil])
        {
            UIAlertView *successDeclineAlert = [[UIAlertView alloc]initWithTitle:@"Заказ успешно отменен"
                                                                         message:nil
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
            successDeclineAlert.tag = 111;
            [successDeclineAlert show];
        }
    } else if (alertView.tag == 111)
    {
        [self configureCellWithOrder:self.order];
    }
}


#pragma mark - IBActions

- (IBAction)actionDeclineOrderButtonClicked:(UIButton *)sender {
    
    UIAlertView *declineAlert = [[UIAlertView alloc]initWithTitle:@"Вы действительно хотите отменить заказ?"
                                                          message:@"Статус заказа будет изменен на 'Отменен'"
                                                         delegate:self
                                                cancelButtonTitle:@"Отмена"
                                                otherButtonTitles:@"Да", nil];
    declineAlert.tag = 123;
    [declineAlert show];
}

@end
