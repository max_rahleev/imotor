//
//  AutoCollectionViewCell.h
//  iMotor
//
//  Created by Maksim Rakhleev on 04.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIScrollView  *photosScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel       *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel       *modelLabel;
@property (weak, nonatomic) IBOutlet UITextView    *infoTextView;
@property (weak, nonatomic) IBOutlet UILabel       *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton      *createOrderButton;

@property (strong, nonatomic) Model *model;
@property (strong, nonatomic) UIViewController *parentViewController;

- (void)configureCellWithModel:(Model *)model;

- (IBAction)actionCreateOrderButtonClicked:(UIButton *)sender;

@end
