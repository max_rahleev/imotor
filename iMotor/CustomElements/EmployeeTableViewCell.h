//
//  EmployeeTableViewCell.h
//  iMotor
//
//  Created by Maksim Rakhleev on 05.04.15.
//  Copyright (c) 2015 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UpdateTableBlock)();

@interface EmployeeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *fioAndTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property (weak, nonatomic) IBOutlet UIButton *deleteUserButton;

- (IBAction)actionDeleteUserFromDBButtonClicked:(UIButton *)sender;

@property (strong, nonatomic) User *user;
@property (copy, nonatomic) UpdateTableBlock updateTableBlock;

- (void)configureCellWithUser:(User *)user;

@end
